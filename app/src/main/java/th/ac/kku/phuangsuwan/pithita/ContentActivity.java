package th.ac.kku.phuangsuwan.pithita;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

public class ContentActivity extends AppCompatActivity implements View.OnClickListener {
    String text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);


        TextView tv_green = (TextView)findViewById(R.id.green);
        tv_green.setText("Green");
        tv_green.setTextSize(30);
        tv_green.setGravity(Gravity.CENTER);
        //tv_green.(Gravity.CENTER_VERTICAL);
        tv_green.setOnClickListener(this);


    }


    @Override
    public void onClick(View view) {
    if(view.getId()==R.id.green) {
        Intent intent = new Intent(getApplication(), MainActivity.class);
        startActivity(intent);
    }
    }
}
